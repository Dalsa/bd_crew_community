# Generated by Django 3.0.3 on 2020-09-12 14:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('community', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='head_image',
            field=models.ImageField(blank=True, upload_to='community/%Y/%m/%d/'),
        ),
    ]
