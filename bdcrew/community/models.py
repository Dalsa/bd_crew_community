from django.db import models
from django.contrib.auth.models import User
from markdownx.models import MarkdownxField
from markdownx.utils import markdown
import uuid

class Category(models.Model):
    name = models.CharField(max_length=25, unique=True) #카테고리 중복은 허용하지 않는다.
    description = models.TextField(blank=True)

    slug = models.SlugField(unique=True, allow_unicode=True) #unicode를 허용한다.. 한글..
    #slugfield는 독립성을 가질때 pk를 많이 사용했는데. 이번에는 그렇게 하지 않고 slug를 사용했다.

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/community/category/{}/".format(self.slug)

    class Meta:
        verbose_name_plural = "categories"

class Tag(models.Model):
    name = models.CharField(max_length=30, unique=True)
    slug = models.SlugField(unique=True, allow_unicode=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return "/community/tag/{}/".format(self.slug)

class Post(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=30)
    content = MarkdownxField()

    head_image = models.ImageField(upload_to="community/%Y/%m/%d/", blank=True)

    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE) #검색 더해보세요.

    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL) #사용자 분
    tags = models.ManyToManyField(Tag, blank=True)

    class Meta: #만든 순선대로 ..
        ordering = ['-created',]

    def __str__(self):
        return "{} :: {}".format(self.title, self.author)

    def get_absolute_url(self):
        return "/community/{}/".format(self.pk)

    def get_update_url(self):
        return self.get_absolute_url() + 'update/'

    def get_markdown_content(self):
        return markdown(self.content)

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = MarkdownxField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def get_markdown_content(self):
        return markdown(self.text)

    def get_absolute_url(self):
        return self.post.get_absolute_url() + '#comment-id-{}'.format(self.pk)