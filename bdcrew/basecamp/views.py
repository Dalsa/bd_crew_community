from django.shortcuts import render, redirect

def index(request):
    return redirect('/community/')

def about_me(request):
    return render(
        request,
        'basecamp/about_me.html'
    )